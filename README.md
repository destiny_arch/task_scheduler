# Introduction
Состав команды: 
- Иванов Тимур 
- Гордеева Елена 
- Корнеева Варвара 
- Губайдуллин Ильшат.

**Планер задач** - календарь для планирования и контроля своего распорядка дня в виде веб приложения.

# Какие проблемы решает?
На рынке нет приложения, которое объединяло бы в себе интерфейс календаря и планировщика задач.

# Целевая аудитория
- Школьники 
- Студенты 
- Работники
- Люди, которым нужна организованность

# Функции приложения
- Добавлять задачи;
- Добавлять дедлайны;
- Добавлять оповещения;
- Облачные сохранения;
- Просмотр задач в календаре;
- Назначать задачи другому человеку.

# Возможности расширения и монетизации
- Интеграция с trello
- Премиум аккаунт


# [Vision](https://docs.google.com/presentation/d/1WRsk9QuiDj1j82mIPQZYV8E32lxxRWoIvDauQRiME00/edit?usp=sharing)

# [Use cases](https://docs.google.com/document/d/1tF3mrd9lcoAWGYT4Td-OTjf6SdAJpryUohsNWo6sPps/edit)

# [Requirements](https://docs.google.com/document/d/1ocfp8KpVZNqZABtQI7Lb3Q_waRnTMY7-nA2-BP5DIIE/edit)

# [Диаграмма Ганта](https://drive.google.com/file/d/1Touo_rKndhsIf3egZ_r9Ac1W17Qf_aqv/view?usp=sharing)

# [Test plan](https://docs.google.com/document/d/1ychlvtLT4Xh_xz8hsJr6k_1imbe-roAPALkb1OVyCRE/edit?usp=sharing)

# [Test Cases](https://docs.google.com/document/d/1o-F0p3BFnSEZ8fifOu7cjOy4UA0qZ6hxpt59xsPp2Jo/edit?usp=sharing)

# Архитектура

![img.png](img.png)
![HLD-2](diagram.jpg)
![HLD-DB](HLD-БД.jpg)
